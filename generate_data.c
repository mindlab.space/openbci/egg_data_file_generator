/*
	generate a fake EEG signal
	compilation: gcc generate_data.c -lm
	usage: ./a.out > test.txt
*/

#include <stdio.h>
#include <math.h>

double fchannel1(int t){
	return 1000*cos(2*2.0*1.0*M_PI*((double)t)/1000);
};

double fchannel2(int t){
	return 1000*cos(2*2.0*1.0*M_PI*((double)t)/1000);
};

double fchannel3(int t){
	return 1000*cos(2*2.0*1.0*M_PI*((double)t)/1000);
};

double fchannel4(int t){
	return 1000*cos(2*2.0*1.0*M_PI*((double)t)/1000);
};

int main(){
	// FILE* file; // output stream

	// data parameters...
	int isample = 0; // sample index
	int ichunk = 0; // chunk index
	int nsample = 200; // number of samples per chunk
	int nchunk = 60; // number of chunks
	double dt = 0.005; // samling time (?)
	// ... and variables
	double channel1 = 0;
	double channel2 = 0;
	double channel3 = 0;
	double channel4 = 0;
	double xaxis = 0;
	double yaxis = 0;
	double zaxis = 0;

	// times
	int t0 = (8*60*60+26*60+2)*1000+0; // starting time 08:26:02.000
	int t = 0; // timestamp
	int hour, min, sec, msec; // for conversion timestamp -> hour:min:sec.msec

	// file = fopen("test.txt", "w"); // open file
	// this header is necessary
	printf("%%OpenBCI Raw EEG Data\n%%Number of channels = 4\n%%Sample Rate = 200.0 Hz\n%%First Column = SampleIndex\n%%Last Column = Timestamp\n%%Other Columns = EEG data in microvolts followed by Accel Data (in G) interleaved with Aux Data)\n");
	int k = 0;
	for(ichunk = 0; ichunk < nchunk; ichunk++){
		for(isample = 0; isample < nsample; isample++){
			t = (int) k*dt*1000+t0;
			msec = t % 1000;
			sec = ( t / 1000 ) % 60;
			min = ( t / 60000 ) % 60;
			hour = ( t / 3600000) % 24;
			channel1 = fchannel1(t);
			channel2 = fchannel2(t);
			channel3 = fchannel3(t);
			channel4 = fchannel4(t);
			printf("%5d, %.2f, %.2f, %.2f, %.2f, %.3f, %.3f, %.3f, %02d:%02d:%02d.%03d, %d\n",
				isample, channel1, channel2, channel3, channel4, xaxis, yaxis, zaxis, hour, min, sec, msec, t);
			k++;
		}
	}
	// fclose(file); // close file
	return 0;
}


